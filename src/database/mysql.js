const mysql = require("mysql");

const pool = mysql.createPool({
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQ_DATABASE,
  host: process.env.MYSQL_HOOST,
  port: process.env.MYSQL_PORT
});

exports.pool = pool;
