const express = require("express");
const routes = express.Router();

const mysql = require("../database/mysql").pool;

//GET
routes.get("/", (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ message: error });

    // const query = "SELECT * FROM pedidos";

    const query = `SELECT pedidos.id_pedido,
                          pedidos.quantidade,
                          produtos.id_produto,
                          produtos.nome,
                          produtos.preco
                     FROM pedidos
               INNER JOIN produtos
                       ON produtos.id_produto =  pedidos.id_produto`;

    conn.query(query, (error, result, field) => {
      conn.release();

      const response = {
        pedidos: result.map(pedido => {
          return {
            id_pedido: pedido.id_pedido,
            quantidade: pedido.quantidade,
            produto: {
              id_produto: pedido.id_produto,
              nome: pedido.nome,
              preco: pedido.preco
            },
            request: {
              tipo: "GET",
              descricao: "Retorna os produtos cadastrados"
            }
          };
        })
      };

      if (error) return res.status(500).json({ message: error });

      return res.status(200).json(response);
    });
  });
});

//POST
routes.post("/", (req, res) => {
  const { id_produto, quantidade } = req.body;

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ message: error });

    const query = `INSERT INTO pedidos (id_produto, quantidade) VALUES ('${id_produto}','${quantidade}')`;

    conn.query(query, (error, result, field) => {
      conn.release();

      if (error) return res.status(500).json({ message: error });

      return res.status(201).json({ result });
    });
  });
});

//GET ONE
routes.get("/:id_pedido", (req, res) => {
  const { id_pedido } = req.params;

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ message: error });

    const query = `SELECT * FROM pedidos WHERE id_pedido=${id_pedido}`;

    conn.query(query, (error, result, field) => {
      conn.release();

      if (error) return res.status(500).json({ message: error });

      return res.status(200).json(result);
    });
  });
});

//PATCH
routes.patch("/:id_pedido", (req, res) => {
  const { id_pedido } = req.params;
  const { quantidade, id_produto } = req.body;
  console.log(req.body);

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ message: error });

    const query = `UPDATE pedidos SET quantidade=${quantidade}, id_produto=${id_produto} WHERE id_produto=${id_pedido}`;

    conn.query(query, (error, result, field) => {
      conn.release();

      console.log(result);

      if (error) return res.status(500).json({ message: error });

      return res.status(202).json({ message: "Pedido alterado com sucesso!" });
    });
  });
});

//DELETE
routes.delete("/:id_pedido", (req, res) => {
  const { id_pedido } = req.params;

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ message: error });

    const query = `DELETE FROM pedidos WHERE id_pedido = ${id_pedido}`;

    conn.query(query, (error, result, fields) => {
      conn.release();

      if (error) return res.status(500).json({ message: error });

      return res.status(202).json({ message: "Pedido excluido com sucesso" });
    });
  });
});

module.exports = routes;
