// PUT => alteração do dado será com referência a entidade completa
// PATCH =>  usado para atualização parcial

/*
1 - Pegar conexão
2 - Query
3 - Abrir conexão
*/

const express = require("express");
const routes = express.Router();

const mysql = require("../database/mysql").pool;

const multer = require("multer");

const login = require("../middlewares/login");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 5 },
  fileFilter: fileFilter
});

//GET
routes.get("/", (req, res) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).json({ error });
    }

    const query = `SELECT * FROM produtos`;
    conn.query(query, (error, result, field) => {
      conn.release();

      if (error) return res.status(500).json({ error, response: null });

      const response = {
        quantidade: result.length,
        produtos: result.map(prod => {
          return {
            id_produto: prod.id_produto,
            nome: prod.nome,
            preco: prod.preco,
            request: {
              tipo: "GET",
              descricao: "",
              url: "http://localhost:3333/produtos/" + prod.id_produto
            }
          };
        })
      };

      return res.status(200).json(response);
    });
  });
});

//POST
routes.post("/", login, upload.single("produto_imagem"), (req, res) => {
  const { nome, preco } = req.body;
  const { image_produto } = req.file.path;

  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).json({ error });
    }

    const query = `INSERT INTO produtos (nome, preco, image_produto) VALUES ('${nome}', '${preco}', '${image_produto}')`;
    conn.query(query, (error, result, field) => {
      conn.release();

      if (error) {
        return res.status(500).json({ error: error, response: null });
      }

      return res.status(201).json({
        mensagem: "Produto inserido com sucesso!",
        id_produto: result.insertId
      });
    });
  });
});

//GET
routes.get("/:id_produto", (req, res) => {
  const { id_produto } = req.params;

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ error });

    const query = `SELECT * FROM produtos WHERE id_produto = ${id_produto}`;

    conn.query(query, (error, result, field) => {
      conn.release();

      if (error) return res.status(500).json({ error, response: null });

      return res.status(201).json({ result });
    });
  });
});

//PATCH
routes.patch("/:id_produto", (req, res) => {
  const { id_produto } = req.params;
  const { nome, preco } = req.body;

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ error });

    const query = `UPDATE produtos SET nome='${nome}', preco='${preco}' WHERE id_produto=${id_produto}`;
    conn.query(query, (error, result, field) => {
      conn.release();

      if (error) return res.status(500).json({ error, response: null });

      return res.status(202).json({ message: "Alterado com sucesso!" });
    });
  });
});

//DELETE
routes.delete("/:id_produto", (req, res) => {
  const { id_produto } = req.params;

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ error });

    const query = `DELETE FROM produtos WHERE id_produto = ${id_produto}`;

    conn.query(query, (error, result, field) => {
      if (error) return res.status(500).json({ error });

      return res.status(202).json({ message: "Removido com sucesso" });
    });
  });
});

module.exports = routes;
