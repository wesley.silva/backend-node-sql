const express = require("express");
const router = express.Router();
const mysql = require("../database/mysql").pool;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

router.post("/cadastro", (req, res, next) => {
  const { email, senha } = req.body;

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ message: error });

    //verificando se email ja existe na base de dados
    const query = `SELECT * FROM usuarios WHERE email = '${email}'`;
    conn.query(query, (error, result) => {
      if (error) return res.status(500).json({ message: error });

      if (result.length > 0) {
        return res.status(401).json({ message: "Usuário já cadastrado" });
      } else {
        bcrypt.hash(senha, 10, (errorBcrypt, hash) => {
          if (errorBcrypt)
            return res.status(500).json({ message: errorBcrypt });

          const query = `INSERT INTO usuarios (email, senha) VALUES ('${email}', '${hash}')`;

          conn.query(query, (error, result, field) => {
            conn.release();

            if (error) return res.status(500).json({ message: error });

            return res
              .status(201)
              .json({ message: "Usuario cadastrado com sucesso!" });
          });
        });
      }
    });
  });
});

router.post("/login", (req, res, next) => {
  const { email, senha } = req.body;

  mysql.getConnection((error, conn) => {
    if (error) return res.status(500).json({ message: error });

    const query = `SELECT * FROM usuarios WHERE email = '${email}'`;

    conn.query(query, (error, result, field) => {
      conn.release();

      if (error) return res.status(500).json({ message: error });

      if (result < 1)
        return res.status(401).send({ message: "Falha na autenticação" });

      bcrypt.compare(senha, result[0].senha, (error, result) => {
        if (error)
          return res.status(401).send({ message: "Falha na autenticação" });

        console.log(result);

        if (result) {
          const token = jwt.sign(
            {
              id_usuario: result[0].id_usuario,
              email: result[0].email
            },
            process.env.JWT_KEY,
            {
              expiresIn: "1h"
            }
          );

          return res
            .status(200)
            .json({ message: "Autenticado com sucesso", token: token });
        }
        return res.status(401).json({ message: "Falha na autenticação" });
      });
    });
  });
});

module.exports = router;
