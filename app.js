// control + e

const express = require("express");
const morgan = require("morgan");

const app = express();

//Router de produtos
const rotaProdutos = require("./src/routes/produtos");

//Router de pedidos
const rotaPedidos = require("./src/routes/pedidos");

//Router de usuarios
const rotaUsuarios = require("./src/routes/usuarios");

app.use(express.json());
app.use(morgan("dev")); //dev = ambiente de desenvolvimento

app.use("/uploads", express.static("uploads"));

//cors
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Header",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );

  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "GET, PATCH, POST, PUT, DELETE");
    return res.status(200);
  }
  next();
});

app.use("/usuarios", rotaUsuarios);
app.use("/produtos", rotaProdutos);
app.use("/pedidos", rotaPedidos);

//Quando o node não encontrar a rota
app.use((req, res, next) => {
  const erro = new Error("Não encontrada a rota");
  erro.status = 404;
  next(erro);
});

app.use((erro, req, res, next) => {
  return res.status(erro.status || 500).json({ erro: erro.message }); //erro.status e erro.message veio como parametro do next().
});

module.exports = app;
